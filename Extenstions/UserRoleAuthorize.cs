﻿using fronk.Models;
using Microsoft.AspNetCore.Authorization;
using System;

namespace fronk.Extenstions
{
    public class UserRoleAuthorize : AuthorizeAttribute
    {
        public UserRoleAuthorize(params UserRole[] roles) : base()
        {
            Roles = String.Join(",", roles);
        }
    }
}
