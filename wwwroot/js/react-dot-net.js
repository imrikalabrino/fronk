﻿// Effect Hook

function useEffect(callback, dependencies) {
    var effect = { callback, dependencies };
    if (useEffect.caller.useEffects) {
        useEffect.caller.useEffects.push(effect);
    } else {
        useEffect.caller.useEffects = [effect];
    }
}

// State Hook

function useState(val) {
    var state = new State(useState.caller, val);

    return [state.get, state.set];
}

function State(ref, val) {
    this.val = val;
    this.subscriber = ref;

    this.set = (val) => {
        this.val = val;
        this.subscriber.render(this);
    }

    this.get = () => {
        return this.val;
    }
}


// Life Cycle Hooks

function dispatchHook(renderRef, rootElemId) {
    var { caller } = dispatchHook;

    caller.useEffects?.forEach(effect => {
        if (!effect.dependencies) {
            effect.callback();
        }
    });

    caller.render = (state) => {
        caller.useEffects?.forEach(effect => {
            var { dependencies } = effect;
            if (dependencies && dependencies.length > 0 && dependencies.indexOf(state.get) != -1) {
                effect.callback();
            }
        });

        clearHook(rootElemId);
        renderRef();
    }

    renderRef();
}

function clearHook(elemId) {
    $(`#${elemId}`)?.find('> *').remove();
}