const weatherElementSelector = '#weather';

const formatDate = date => {
    const hours = date.getHours().toString().padStart(2, '0');
    const day = date.getDate().toString().padStart(2, '0');
    const month = (date.getMonth() + 1).toString().padStart(2, '0');
    const year = date.getFullYear().toString().padStart(4, '000');
    //YYYY-MM-DD:HH
    return `${year}-${month}-${day}:${hours}`;
}

const getEdgeTimes = (date) => {
    const startTime = new Date(date);
    const endTime = new Date(date);
    endTime.setHours(endTime.getHours() + 1);
    startTime.setHours(startTime.getHours());

    return {
        startTime: formatDate(startTime),
        endTime: formatDate(endTime)
    }
}

async function fetchWeather(lat, lon, date) {
    let message;
    const key = "9024fe93ad5546dbba977d4059bddb76";
    const isFuture = new Date(date).getTime() > new Date().getTime();

    if (isFuture) {
        const maxDaysFuture = 16;
        const datediff = (first, second) => Math.round((second - first) / (1000 * 60 * 60 * 24));
        const diff = datediff(new Date(), new Date(date));

        if (diff > maxDaysFuture) {
            message = `The event is more than ${maxDaysFuture} days into the future! \n check back in in ${diff - 16} days to fetch a forecast!`
        } else {
            const url = "https://api.weatherbit.io/v2.0/forecast/daily";
            const params = new URLSearchParams({
                key, lat, lon, days: diff
            });

            const result = await $.ajax({url: url + "?" + params,})
            const data = result.data[result.data.length - 1];
            console.log(data);
            message = `Expected temp will be ${data.temp}ºC! ${data.weather.description}...`;
        }
    } else {
        const maxYearsPast = 4;
        const diff = new Date().getFullYear() - new Date(date).getFullYear();
        if (diff > maxYearsPast) {
            message = `The event is more than ${maxYearsPast} years into the past! hope you had fun!`
        } else {
            const {startTime, endTime} = getEdgeTimes(date);
            const url = "https://api.weatherbit.io/v2.0/history/hourly";
            const params = new URLSearchParams({
                key, lat, lon, start_date: startTime, end_date: endTime,
            });

            const result = await $.ajax({url: url + "?" + params,});
            const data = result.data[0];
            console.log(data);
            message = `Temp was ${data.temp}ºC! ${data.weather.description}...`;
        }
    }

    $(weatherElementSelector).append(message);
}