﻿function renderEventStatistics() {
    var rootElemId = `eventsGraph`;

    var [getIsLoading, setIsLoading] = useState(true);
    var [getData, setData] = useState([]);

    useEffect(() => {
        $.ajax({
            url: "../Administration/GetMostPopularEvents",
            method: "GET",
            success: (result) => {
                var formattedResult = result.reduce((result, country) => {
                    result.push({ name: country.title, value: country.userCount });
                    return result;
                }, []);
                setData(formattedResult);
                setIsLoading(false);
            },
            fail: (err, desc) => {
                console.log(err);
                console.log(desc);
            }
        });
    });

    function render() {
        if (getIsLoading()) {
            renderStatisticsLoading(rootElemId);
        } else {
            renderBarChart(rootElemId, getData());
        }
    }

    dispatchHook(render, rootElemId);
}

function renderOrganizationStatistics() {
    const rootElemId = `organizationsGraph`;

    var [getIsLoading, setIsLoading] = useState(true);
    var [getData, setData] = useState([]);

    useEffect(() => {
        $.ajax({
            url: "../Administration/GetOrganizationsGroupedByCountry",
            method: "GET",
            success: (result) => {
                var formattedResult = result.reduce((result, country) => {
                    result.push({ name: country.name, value: country.count });
                    return result;
                }, []);
                setData(formattedResult);
                setIsLoading(false);
            },
            fail: (err, desc) => {
                console.log(err);
                console.log(desc);
            }
        });
    });

    function render() {
        if (getIsLoading()) {
            renderStatisticsLoading(rootElemId);
        } else {

            renderPieChart(rootElemId, getData());
        }
    }

    dispatchHook(render, rootElemId);
}

function renderStatisticsLoading(elemId) {
    $(`#${elemId}`).append("<div class='loading-graph'>Loading...</div>");
}

function renderBarChart(elemId, data) {
    var margin = ({ top: 20, right: 0, bottom: 30, left: 40 });
    var color = "steelblue";
    var height = 500;
    var width = 900;

    var x = d3.scaleBand()
        .domain(data.map(d => d.name))
        .range([margin.left, width - margin.right])
        .padding(0.1);

    var y = d3.scaleLinear()
        .domain([0, d3.max(data, d => d.value)]).nice()
        .range([height - margin.bottom, margin.top]);

    var xAxis = g => g
        .attr("transform", `translate(0,${height - margin.bottom})`)
        .call(d3.axisBottom(x).tickSizeOuter(0));

    var yAxis = g => g
        .attr("transform", `translate(${margin.left},0)`)
        .call(d3.axisLeft(y))
        .call(g => g.select(".domain").remove());


    var zoom = (svg) => {
        const extent = [[margin.left, margin.top], [width - margin.right, height - margin.top]];

        svg.call(d3.zoom()
            .scaleExtent([1, 8])
            .translateExtent(extent)
            .extent(extent)
            .on("zoom", zoomed));

        function zoomed(event) {
            x.range([margin.left, width - margin.right].map(d => event.transform.applyX(d)));
            svg.selectAll(".bars rect").attr("x", d => x(d.name)).attr("width", x.bandwidth());
            svg.selectAll(".x-axis").call(xAxis);
        }
    }

    var graph = d3.select(`#${elemId}`);
    var svg = graph.append("svg");

    svg.attr("width", width)
        .attr("height", height);

    svg.attr("viewBox", [0, 0, width, height])
        .call(zoom);

    svg.append("g")
        .attr("class", "bars")
        .attr("fill", color)
        .selectAll("rect")
        .data(data)
        .join("rect")
        .attr("x", d => x(d.name))
        .attr("y", d => y(d.value))
        .attr("height", d => y(0) - y(d.value))
        .attr("width", x.bandwidth());

    svg.append("g")
        .attr("class", "x-axis")
        .call(xAxis);

    svg.append("g")
        .attr("class", "y-axis")
        .call(yAxis);

    return svg.node();
}

function renderPieChart(elemId, data) {
    var color = d3.scaleOrdinal()
        .domain(data.map(d => d.name))
        .range(d3.quantize(t => d3.interpolateSpectral(t * 0.8 + 0.1), data.length).reverse());
    var width = 900;
    var height = Math.min(width, 500);
    var arc = d3.arc()
        .innerRadius(0)
        .outerRadius(Math.min(width, height) / 2 - 1);
    var arcLabel = () => {
        const radius = Math.min(width, height) / 2 * 0.8;
        var arc = d3.arc().innerRadius(radius).outerRadius(radius);
        return arc;
    };
    var pie = d3.pie()
        .sort(null)
        .value(d => d.value);

    var graph = d3.select(`#${elemId}`);
    var svg = graph.append("svg");

    const arcs = pie(data);

    svg.attr("viewBox", [-width / 2, -height / 2, width, height]);

    svg.append("g")
        .attr("stroke", "white")
        .selectAll("path")
        .data(arcs)
        .join("path")
        .attr("fill", d => color(d.data.name))
        .attr("d", arc)
        .append("title")
        .text(d => `${d.data.name}: ${d.data.value.toLocaleString()}`);

    svg.append("g")
        .attr("font-family", "sans-serif")
        .attr("font-size", 12)
        .attr("text-anchor", "middle")
        .selectAll("text")
        .data(arcs)
        .join("text")
        .attr("transform", d => `translate(${arcLabel().centroid(d)})`)
        .call(text => text.append("tspan")
            .attr("y", "-0.4em")
            .attr("font-weight", "bold")
            .text(d => d.data.name))
        .call(text => text.filter(d => (d.endAngle - d.startAngle) > 0.25).append("tspan")
            .attr("x", 0)
            .attr("y", "0.7em")
            .attr("fill-opacity", 0.7)
            .text(d => d.data.value.toLocaleString()));

    return svg.node();
}

$(document).ready(() => {
    renderOrganizationStatistics();
    renderEventStatistics();
});