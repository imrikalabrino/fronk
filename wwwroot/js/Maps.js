// import keys from "./keys.js";

function generateGoogleSrcTag (parentSelector)  {
    const generateGoogleSrcString = (key) => {
        return "https://maps.googleapis.com/maps/api/js?key="
            + key
            + "&libraries=places&callback=initMap&channel=GMPSB_addressselection_v1_cAB";
    }
    
    const key = "AIzaSyAtATQ93pyisBzz27mni6thcPQhxM5fFP4";
    // const key = keys.google;
    const my_awesome_script = document.createElement('script');
    my_awesome_script.setAttribute('src', generateGoogleSrcString(key));

    my_awesome_script.setAttribute('async', true);
    my_awesome_script.setAttribute('defer', true);
    
    console.log(parentSelector, my_awesome_script);
    $(parentSelector).append(my_awesome_script);
}