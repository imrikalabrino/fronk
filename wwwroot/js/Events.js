function joinEvent(eventId) {
    $.ajax({
        method: 'post',
        url: '/Events/Join',
        data: { eventId },
        success: (result) => {
            window.location.reload();
        }
    })
    // .done(function (data) {
    //     $('#eventsGrid').html('');
    // })
}
function unjoinEvent(eventId) {
    $.ajax({
        method: 'post',
        url: '/Events/UnJoin',
        data: { eventId },
        success: (result) => {
            window.location.reload();
        }
    })
    // .done(function (data) {
    //     $('#eventsGrid').html('');
    // })
}
// export default joinEvent;
// module.exports.unjoinEvent = unjoinEvent;