$(document).ready(function () {
    var url = $("#mVideo").attr('src');

    $("#myModal").on('hide.bs.modal', function () {
        $("#mVideo").attr('src', '');
    });

    $("#myModal").on('show.bs.modal', function () {
        $("#mVideo").attr('src', url);
    });
});