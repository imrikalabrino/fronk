$(document).ready(() => {
    renderTweets();
});

function renderTweet(root, tweet) {
    const entities = JSON.parse(tweet.rawSource).entities;
    const img = (entities && entities.media && entities.media.length > 0) ? `<div class="tweet-img-wrap"><img src="${entities.media[0].media_url}" alt="" class="tweet-img"></div>` : '';
    $('#' + root).append('<div class="tweet-wrap">' +
                            '<div class= "tweet-header" >' +
                                `<img src="${tweet.user.profileImageUrl}" alt="${tweet.user.screenName}"` +
                                    `title="${tweet.user.screenName}" class="avator" />` +
                                '<div class="tweet-header-info">' +
                                    `${tweet.user.name} <span>@${tweet.user.screenName}</span> <span>${new Date(tweet.createdDate).toDateString()}</span>` +
                                    `<p>${tweet.text}</p>` +
                                '</div>' +
                            '</div>' +
                            img +
                            '<div class="tweet-info-counts">' +
                                `${tweet.user.location}` +
                            '</div>' +
                         '</div>');
}

function renderTweets() {

    var rootId = "tweets";

    $.ajax({
        url: '../Home/TweetView',
        method: 'POST',
    }).then(result => {
        console.log(result);
        result.forEach(tweet => renderTweet(rootId, tweet));
    }).catch(console.error);
}