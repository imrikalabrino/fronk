function joinOrg(e, orgId) {
    e.preventDefault();
    $.ajax({
        method: 'post',
        url: '/Organizations/Join',
        data: { orgId }
    })
    .then(() => window.location.reload())
}
function unjoinOrg(e, orgId) {
    e.preventDefault();
    $.ajax({
        method: 'post',
        url: '/Organizations/UnJoin',
        data: { orgId },
    })
    .then(() => window.location.reload())
}

function renderLoadingOrgs() {

}

function renderMyOrgs() {
    const rootElemId = "joinedOrgs";
    var [getOrgs, setOrgs] = useState([]);
    var [getIsLoading, setIsLoading] = useState(true);

    useEffect(() => {
        $.ajax({
            url: '../Organizations/getJoinedOrganizations',
            success: (result) => {
                setOrgs(result);
                setIsLoading(false);
            }
        });
    });

    function render() {
        if (getIsLoading()) {
            renderLoadingOrgs();
        } else {
            
        }
    }

    dispatchHook(render, rootElemId);
}

$(document).ready(() => {
    $(`#orgAdvancedSearchOptionsToggler`).click(() => {
        var advanceOptionsElem = $(`#orgAdvancedSearchOptions`);

        if (advanceOptionsElem?.hasClass('hidden')) {
            advanceOptionsElem.removeClass('hidden');
        } else {
            advanceOptionsElem?.addClass('hidden');
        }
    });
    
});

