const replaceByKeyVal = (key, value, template, surroundWithBrackets = true) => {
    return template.replaceAll(
        surroundWithBrackets ? '{' + key + '}' : key,
        value);
}

const concatJoiningActions = (event, currUserId) => {
    const {date, users, id} = event;

    const attending = users != null && users.some(user => (user.id.toString() == currUserId));
    const isPastEvent = new Date(date).getTime() < new Date().getTime();
    if (isPastEvent) {
        if (attending) {
            return `<span class="badge badge-success">You attended!</span>`
        }
    } else {
        if (attending) {
            return `<button class="btn details-button" id="attendingBadge"
                            onclick="unjoinEvent(${id})">
                        Unattend
                    </button>`
        } else {
            return `<button id="joinButton" type="button"
                            class="btn action-button"
                            onclick="joinEvent(${id})">
                        Join
                    </button>`
        }
    }

    return false;
}

const concatActions = (event, userRole) => {
    const {id} = event;
    return userRole == "SYS_ADMIN"
        ? `<div>
                <a class="btn btn-sm btn-outline-secondary" href="/Events/Edit?id=${id}">Edit</a>
                <a class="btn btn-sm btn-outline-secondary" href="/Events/Delete?id=${id}">Delete</a>
            </div>`
        : `<div/>`;
}

$(function () {
    $('#titleSearchForm').submit(function (e) {
        e.preventDefault();

        var query = $('#query').val();

        $.ajax({
            url: '/Events/SearchByTitle',
            data: {'query': query}
        })
            .done(function (data) {
                $('#eventsGrid').html('');
                var template = $('#hidden-template').html();

                $.each(data, function (i, val) {
                    var temp = template;

                    $.each(val, function (key, value) {
                        if (key === "catColor") {
                            temp = replaceByKeyVal("black", value, temp, false)
                        } else {
                            temp = replaceByKeyVal(key, value, temp)
                        }
                    });

                    var multiidhtml = $(temp);
                    const joinHTMLData = concatJoiningActions(val, userId);
                    const actionsHTMLData = concatActions(val, userRole);
                    if (joinHTMLData) {
                        multiidhtml.find("#actions").append(joinHTMLData)
                    }

                    if (actionsHTMLData) {
                        multiidhtml.find("#muted-actions").append(actionsHTMLData)
                    }

                    $('#eventsGrid').append(multiidhtml);
                });
            }).fail((error) => {
            console.log(error)
        });
    });
});
