using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using fronk.Models;

namespace fronk.Data
{
    public class fronkContext : DbContext
    {
        public fronkContext (DbContextOptions<fronkContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Organization>().HasOne(o => o.Manager).WithOne(u => u.Organization).HasForeignKey<User>(u => u.OrganizationId).OnDelete(DeleteBehavior.Cascade).IsRequired(false);
            modelBuilder.Entity<User>().HasOne(u => u.Organization).WithOne(o => o.Manager).HasForeignKey<Organization>(o => o.ManagerId).OnDelete(DeleteBehavior.SetNull);
            modelBuilder.Entity<UserOrganization>().HasKey(uo => new { uo.UserId, uo.OrganizationId });
            modelBuilder.Entity<UserOrganization>().HasOne(uo => uo.User).WithMany(u => u.UserOrganizations).HasForeignKey(uo => uo.UserId).OnDelete(DeleteBehavior.NoAction);
            modelBuilder.Entity<UserOrganization>().HasOne(uo => uo.Organization).WithMany(o => o.UserOrganizations).HasForeignKey(uo => uo.OrganizationId).OnDelete(DeleteBehavior.NoAction);
        }

        public DbSet<fronk.Models.Event> Event { get; set; }
        public DbSet<fronk.Models.User> User { get; set; }
        public DbSet<fronk.Models.EventCategory> EventCategory { get; set; }
        public DbSet<fronk.Models.Organization> Organization { get; set; }
        public DbSet<fronk.Models.Location> Location { get; set; }
    }
}

        