using System.Collections.Generic;
using fronk.Models;
using Microsoft.AspNetCore.Http;

namespace fronk.Services
{
    public interface IUsersService
    {
         int? getCurrentLoggedInUserId(HttpContext httpContext);
         User getCurrentLoggedInUser(HttpContext httpContext);
    }
}