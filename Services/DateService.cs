using System;

namespace fronk.Services
{
    public class DateService
    {
        public static bool isSameDay(DateTime first, DateTime second)
        {
            return
                first.Day == second.Day
                && first.Month == second.Month
                && first.Year == second.Year;
        }
    }
}