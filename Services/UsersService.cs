using System;
using System.Linq;
using System.Security.Claims;
using fronk.Data;
using fronk.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;

namespace fronk.Services
{
    public class UsersService : IUsersService
    {
        private readonly fronkContext _context;

        public UsersService(fronkContext context)
        {
            _context = context;
        }
        
        private IQueryable<User> includeAllForeignEventsTables(IQueryable<User> query)
        {
            return query
                .Include(u => u.UserOrganizations)
                .ThenInclude(orgs => orgs.Organization.Events)
                .ThenInclude(e => e.EventCategory)

                .Include(u => u.UserOrganizations)
                .ThenInclude(orgs => orgs.Organization.Events)
                .ThenInclude(e => e.Location)

                .Include(u => u.UserOrganizations)
                .ThenInclude(orgs => orgs.Organization.Events)
                .ThenInclude(e => e.Organization)

                .Include(u => u.Organization)

                .Include(e => e.Events).ThenInclude(e => e.EventCategory)
                .Include(e => e.Events).ThenInclude(e => e.Location)
                .Include(e => e.Events).ThenInclude(e => e.Organization);
        }

        public int? getCurrentLoggedInUserId(HttpContext httpContext)
        {
            var idAsString = httpContext?.User?.FindFirstValue(ClaimTypes.NameIdentifier);
            if (idAsString != null)
            {
                return Int32.Parse(idAsString);
            }
            
            return null;
        }

        public User getCurrentLoggedInUser(HttpContext httpContext)
        {
            int? currentUserId = getCurrentLoggedInUserId(httpContext);
            if (currentUserId == null)
            {
                return null;
            }
            return _context.User
                .Include(u => u.Organization)
                .Include(u => u.UserOrganizations)
                .FirstOrDefault(u => u.Id == currentUserId);
        }

        public User getCurrentLoggedInUserExtendedIncludes(HttpContext httpContext)
        {
            int? currentUserId = getCurrentLoggedInUserId(httpContext);
            if (currentUserId == null)
            {
                return null;
            }
            return _context.User
                .Include(u => u.UserOrganizations)
                .ThenInclude(orgs => orgs.Organization.Events)
                .Include(u => u.Events)
                .Include(u => u.Organization)
                .FirstOrDefault(u => u.Id == currentUserId);
        }
        
        public User getCurrentLoggedInUserWithEvents(HttpContext httpContext)
        {
            int? currentUserId = getCurrentLoggedInUserId(httpContext);
            if (currentUserId == null)
            {
                return null;
            }
            return includeAllForeignEventsTables(_context.User)
                .FirstOrDefault(u => u.Id == currentUserId);
        }
    }
}