using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using fronk.Data;
using fronk.Models;
using fronk.Services;

namespace fronk.Controllers
{
    public class EventsController : Controller
    {
        private readonly fronkContext _context;
        private readonly UsersService _usersService;

        public EventsController(fronkContext context)
        {
            _context = context;
            _usersService = new UsersService(context);
        }

        public async Task<ActionResult> Search(
            int[]? categoryId, int[]? organizationId,
            string? date
            )
        {
            DateTime? parsedDate = date != null ? DateTime.Parse(date) : null;
            
            var query = from evnt in _context.Event
                where
                    (parsedDate == null ||
                     (evnt.Date.Day == parsedDate.Value.Day
                      && evnt.Date.Month == parsedDate.Value.Month
                      && evnt.Date.Year == parsedDate.Value.Year)) &&
                    (categoryId == null || categoryId.Length == 0 ||
                    categoryId.Contains(evnt.EventCategory.Id)) &&
                    (organizationId == null ||  organizationId.Length == 0 ||
                     organizationId.Contains(evnt.Organization.Id))
                select evnt;

            List<Event> events = await includeAllForeign(query).ToListAsync();

            ViewData["organizations"] = new MultiSelectList(_context.Organization,
                nameof(Organization.Id),
                nameof(Organization.Name), organizationId);
            ViewData["categories"] = new MultiSelectList(_context.EventCategory,
                nameof(EventCategory.Id),
                nameof(EventCategory.Name), categoryId);
            ViewData["date"] = date;

            return View(nameof(Index), events);
        }


        private IQueryable<Event> includeAllForeign(IQueryable<Event> query)
        {
            return query.Include(e => e.EventCategory)
                .Include(e => e.Location)
                .Include(e => e.Organization)
                .Include(e => e.Users);
        }

        private async Task<List<Event>> getAllEventsList()
        {
            return await includeAllForeign(_context.Event).ToListAsync();
        }

        public async Task<IActionResult> Index()
        {
            ViewData["organizations"] = new MultiSelectList(_context.Organization,
                nameof(Organization.Id),
                nameof(Organization.Name));
            ViewData["categories"] = new MultiSelectList(_context.EventCategory,
                nameof(EventCategory.Id),
                nameof(EventCategory.Name));

            return View(await getAllEventsList());
        }

        public async Task<IActionResult> SearchByTitle(string query)
        {
            query ??= "";

            var events = (
                from e in _context.Event
                join cat in _context.EventCategory
                    on e.EventCategoryId equals cat.Id
                join loc in _context.Location
                    on e.Id equals loc.EventId
                join org in _context.Organization
                    on e.OrganizationId equals org.Id
                where e.Title.Contains(query)
                select new
                {
                    id = e.Id,
                    date = e.Date,
                    title = e.Title,
                    description = e.Description,
                    imageSrc = e.ImageSrc,
                    catId = cat.Id,
                    catName = cat.Name,
                    catColor = cat.Color,
                    orgName = org.Name,
                    locName = loc.Name,
                    attendeesCount = e.Users.Count,
                    users = e.Users,
                }).ToList();

            return Json(events);
        }

        private List<User> addUsersAndGetList(Event e, User u)
        {
            List<User> users;
            if (e.Users == null)
            {
                users = new List<User>();
            }
            else
            {
                users = e.Users;
            }

            users.Add(u);

            return users;
        }

        private List<User> removeUserAndGetList(Event e, User u)
        {
            List<User> users = e.Users;
            if (users.Any(us => us.Id == u.Id))
            {
                users.Remove(u);
            }

            return users;
        }

        [HttpPost]
        public async Task<IActionResult> Join(int? eventId)
        {
            if (eventId == null || !EventExists((int) eventId))
            {
                return NotFound();
            }

            var currentUser = _usersService.getCurrentLoggedInUser(HttpContext);

            if (currentUser == null)
            {
                return Unauthorized();
            }

            var eventToJoin = await _context.Event
                .Include(e => e.Users)
                .FirstOrDefaultAsync(e => e.Id == eventId);
            List<User> updatedUsers = addUsersAndGetList(eventToJoin, currentUser);
            eventToJoin.Users = updatedUsers;

            if (ModelState.IsValid)
            {
                try
                {
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    throw;
                }

                return RedirectToAction(nameof(Index));
            }

            return View("Details", eventToJoin);
        }

        [HttpPost]
        public async Task<IActionResult> UnJoin(int? eventId)
        {
            var currentUser = _usersService.getCurrentLoggedInUser(HttpContext);

            if (currentUser == null)
            {
                return Unauthorized();
            }

            if (eventId == null || !EventExists((int) eventId))
            {
                return NotFound();
            }

            var eventToJoin = await _context.Event
                .Include(e => e.Users)
                .FirstOrDefaultAsync(e => e.Id == eventId);

            List<User> updatedUsers = removeUserAndGetList(eventToJoin, currentUser);
            eventToJoin.Users = updatedUsers;

            if (ModelState.IsValid)
            {
                try
                {
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    throw;
                }

                return RedirectToAction(nameof(Index), await getAllEventsList());
            }

            return View("Details", eventToJoin);
        }

        public async Task<IActionResult> Category(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var fronkContext =
                includeAllForeign(_context.Event.Where(e => e.EventCategoryId == id));

            ViewBag.CategoryView = true;
            return View("Index", await fronkContext.ToListAsync());
        }

        // GET: Events/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var @event = await includeAllForeign(_context.Event)
                .FirstOrDefaultAsync(m => m.Id == id);

            if (@event == null)
            {
                return NotFound();
            }

            return View(@event);
        }

        public async Task<IActionResult> Create()
        {
            var currentManager = _usersService.getCurrentLoggedInUser(HttpContext);
            bool isSysAdmin = currentManager.UserRole == UserRole.SYS_ADMIN;
            if (currentManager == null)
                return NotFound();

            Organization currentManagedOrg = currentManager.Organization;
            if (!isSysAdmin
                && (currentManagedOrg == null || currentManagedOrg.ManagerId != currentManager.Id))
                return Unauthorized();

            if (isSysAdmin)
            {
                ViewData["Orgs"] = new SelectList(_context.Organization,
                    nameof(Organization.Id),
                    nameof(Organization.Name));
                ViewData["EventCategoryId"] = new SelectList(_context.EventCategory, nameof(EventCategory.Id),
                    nameof(EventCategory.Name));
                return View();
            }

            Event model = new Event();
            model.OrganizationId = currentManagedOrg.Id;
            model.Organization = currentManagedOrg;
            model.CreatedAt = DateTime.Now;

            ViewData["Country"] = currentManagedOrg.Country;
            ViewData["OrgName"] = currentManagedOrg.Name;
            ViewData["EventCategoryId"] = new SelectList(_context.EventCategory, nameof(EventCategory.Id),
                nameof(EventCategory.Name));
            return View(model);
        }

        // POST: Events/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(
            [Bind("Id,OrganizationId,Title,Description,EventCategoryId,CreatedAt,Date,ImageSrc")]
            Event @event, string placeId, string placeName)
        {
            var currentUser = _usersService.getCurrentLoggedInUser(HttpContext);
            bool validLocationFields = placeId != null && placeName != null;

            if (currentUser == null)
            {
                return Unauthorized();
            }
            
            if (validLocationFields)
                addLocationAndUpdateEvent(@event, placeId, placeName);

            ModelState.Remove("Location");

            if (TryValidateModel(@event, nameof(Event)))
            {
                if (string.IsNullOrEmpty(@event.ImageSrc))
                {
                    @event.ImageSrc =
                        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSTPgIm4NrAPmXKCFcuRa7HBnn1Pu5NclBKgQ&usqp=CAU";
                }

                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }

            ViewData["EventCategoryId"] = new SelectList(_context.EventCategory, nameof(EventCategory.Id),
                nameof(EventCategory.Name), nameof(EventCategory.Name));
            ViewData["OrgName"] = currentUser.Organization.Name;
            return View(@event);
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var @event = await _context.Event
                .Include(e => e.Location)
                .Include(e => e.Organization)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (@event == null)
            {
                return NotFound();
            }

            ViewData["EventCategoryId"] = new SelectList(_context.EventCategory, nameof(EventCategory.Id),
                nameof(EventCategory.Name), @event.EventCategoryId);
            ViewData["OrgName"] = @event.Organization.Name;
            return View(@event);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id,
            [Bind("Id,OrganizationId,Title,Description,EventCategoryId,CreatedAt,Date,ImageSrc")]
            Event @event, string placeId, string placeName)
        {
            var currentUser = _usersService.getCurrentLoggedInUser(HttpContext);
            if (currentUser == null)
            {
                return Unauthorized();
            }

            if (id != @event.Id)
            {
                return NotFound();
            }

            bool validLocationFields = placeId != null && placeName != null;
            // Event prevEvent = await _context.Event
            //     .Include(e => e.Location)
            //     .FirstOrDefaultAsync(e => e.Id == @event.Id);
            //
            ModelState.Remove("Location");
            // @event.Location = prevEvent.Location;

            if (ModelState.IsValid)
            {
                if (string.IsNullOrEmpty(@event.ImageSrc))
                {
                    @event.ImageSrc =
                        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSTPgIm4NrAPmXKCFcuRa7HBnn1Pu5NclBKgQ&usqp=CAU";
                }

                try
                {
                    if (!string.IsNullOrEmpty(placeId))
                    {
                        Location location = _context.Location.FirstOrDefault(loc => loc.EventId == @event.Id);
                        location.Name = placeName;
                        location.PlaceId = placeId;
                        _context.Update(location);
                        await _context.SaveChangesAsync();
                    }
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!EventExists(@event.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }

                return RedirectToAction(nameof(Index));
            }

            ViewData["EventCategoryId"] = new SelectList(_context.EventCategory, nameof(EventCategory.Id),
                nameof(EventCategory.Name), @event.EventCategoryId);
            ViewData["OrgName"] = currentUser.Organization.Name;
            return View(@event);
        }

        // GET: Events/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var @event = await includeAllForeign(_context.Event)
                .FirstOrDefaultAsync(m => m.Id == id);

            if (@event == null)
            {
                return NotFound();
            }

            return View(@event);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var @event = await _context.Event.FindAsync(id);
            _context.Event.Remove(@event);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool EventExists(int id)
        {
            return _context.Event.Any(e => e.Id == id);
        }

        private void addLocation(Event e, string placeId, string placeName)
        {
            Location location = new Location();
            location.PlaceId = placeId;
            location.Name = placeName;
            location.Event = e;
            location.EventId = e.Id;

            _context.Location.Add(location);
            e.Location = location;
        }
        private void addLocationAndUpdateEvent(Event e, string placeId, string placeName)
        {
            addLocation(e, placeId, placeName);
            _context.Event.Add(e);
        }
    }
}