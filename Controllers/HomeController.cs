﻿using fronk.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using fronk.Data;
using fronk.Services;
using Microsoft.EntityFrameworkCore;
using TweetSharp;

namespace fronk.Controllers
{
    public class HomeController : Controller
    {
        public static string _ConsumerKey = "EaiS1nNxlTxUyUXidnkOMV2xM";
        public static string _ConsumerSecret = "gYxC8HdsAghD6Eq3bdQngxtQLarKhncKif0V2b4X9SQML4Rvca";
        public static string _AccessToken = "2588449584-NCn8Qnx6ABoJOHG8tRMzpGUaXOz9Zvp9JQW3kpc";
        public static string _AccessTokenSecret = "TiwySoA3kPPuoZYHdW8DBjxdCDtNmS1RWZIjA7ybQOc9t";

        public static string _BearerToken =
            "AAAAAAAAAAAAAAAAAAAAAG32TAEAAAAADWYi37c0wwoxHTe80Q1KUmg1Dao%3DjEulI9dg3YBc9LJcUu9Zdsu8y0mucQVDVbJ2Ty7TCX5bqOlmUm";

        private readonly ILogger<HomeController> _logger;
        private readonly fronkContext _context;
        private readonly UsersService _usersService;

        public HomeController(ILogger<HomeController> logger, fronkContext context)
        {
            _logger = logger;
            _context = context;
            _usersService = new UsersService(context);
        }

        public IActionResult Index()
        {
            var currentUser = _usersService.getCurrentLoggedInUserWithEvents(HttpContext);
            bool loggedIn = currentUser != null;
            ViewBag.LoggedInView = loggedIn;

            if (loggedIn)
            {
                List<Event> userEvents = currentUser.Events.ToList();


                List<Event> userOrgEvents = currentUser.UserOrganizations.Aggregate(new List<Event>(),
                    (events, userOrganization) =>
                    {
                        foreach (var organizationEvent in userOrganization.Organization.Events)
                            events.Add(organizationEvent);
                        return events;
                    });

                dynamic homeModel = new ExpandoObject();
                homeModel.UserEvents = userEvents;
                homeModel.OrgEvents = userOrgEvents;

                return View(homeModel);
            }

            return View();
        }

        [Authorize]
        public IActionResult Privacy()
        {
            return View();
        }


        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel {RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier});
        }

        [HttpPost]
        public IEnumerable<TwitterStatus> TweetView()
        {
            List<EventCategory> categories = _context.EventCategory.ToList();

            string categoriesQuery = categories.Aggregate(new StringBuilder(),
                (acc, curr) =>
                {
                    string message = acc.Length > 0 ? (" OR " + curr.Name) : curr.Name;
                    acc.Append(message);
                    return acc;
                }).ToString();

            var service = new TwitterService(_ConsumerKey, _ConsumerSecret);

            //AuthenticateWith("Access Token", "AccessTokenSecret");
            service.AuthenticateWith(_AccessToken, _AccessTokenSecret);

            TwitterSearchResult tweets = service.Search(new SearchOptions
            {
                Q = categoriesQuery,
                Resulttype = TwitterSearchResultType.Popular,
                Count = 50
            });
            IEnumerable<TwitterStatus> status = tweets.Statuses;

            return status;
        }
    }
}