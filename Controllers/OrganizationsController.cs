﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using fronk.Data;
using fronk.Models;
using fronk.Services;
using Microsoft.AspNetCore.Authorization;

namespace fronk.Controllers
{
    public class OrgnizationForUser
    {
        public IEnumerable<Organization> Owned { get; set; }

        public IEnumerable<Organization> Follwing { get; set; }

        public IEnumerable<Organization> Others { get; set; }

    }

    public class OrganizationsController : Controller
    {
        private readonly fronkContext _context;
        private readonly UsersService _usersService;

        public OrganizationsController(fronkContext context)
        {
            _context = context;
            _usersService = new UsersService(context);
        }

        public async Task<OrgnizationForUser> GetUserOrgs()
        {
            User curUser = _usersService.getCurrentLoggedInUser(HttpContext);
            IEnumerable<Organization> Owned = await _context.Organization.Where(org => curUser != null && org.Manager.Id == curUser.Id).ToListAsync();
            IEnumerable<Organization> Joined = await getJoinedOrganizations();
            IEnumerable<Organization> Unjoined = await getUnJoinedOrganizations();
            OrgnizationForUser result = new OrgnizationForUser
            {
                Owned = Owned,
                Follwing = Joined,
                Others = Unjoined
            };

            return result;
        }

        public async Task<IEnumerable<Organization>> getJoinedOrganizations()
        {
            int? curUserId = _usersService.getCurrentLoggedInUserId(HttpContext);
            IEnumerable<Organization> organizations = new List<Organization>();

            if (curUserId != null)
            {
                var fronkContext = _context.Organization
                .Include(o => o.Manager)
                .Include(o => o.UserOrganizations)
                .ThenInclude(uo => uo.User)
                .Where(org => org.UserOrganizations.Any(uo => uo.UserId == curUserId));

                organizations = await fronkContext.ToListAsync();
            }

            return organizations;
        }

        public async Task<IEnumerable<Organization>> getUnJoinedOrganizations()
        {
            int? curUserId = _usersService.getCurrentLoggedInUserId(HttpContext);
            IEnumerable<Organization> organizations = new List<Organization>();

            if (curUserId != null)
            {
                var fronkContext = _context.Organization
                    .Include(o => o.Manager)
                    .Include(o => o.UserOrganizations)
                    .ThenInclude(uo => uo.User)
                     .Where(org => org.ManagerId != curUserId && (org.UserOrganizations.Count() == 0 || org.UserOrganizations.All(uo => uo.UserId != curUserId)));

                organizations = await fronkContext.ToListAsync();
            }
            return organizations;
        }

        public async Task<IActionResult> Index()
        {
            var fronkContext = _context.Organization
                .Include(o => o.Manager)
                .Include(o => o.UserOrganizations)
                .ThenInclude(uo => uo.User);
            List<Organization> organizations = await fronkContext.ToListAsync();
            OrgnizationForUser orgs = await GetUserOrgs();

            return View(orgs);
        }

        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var organization = await _context.Organization
                .Include(o => o.Manager)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (organization == null)
            {
                return NotFound();
            }

            return View(organization);
        }

        public IActionResult Create()
        {
            var currentUser = _usersService.getCurrentLoggedInUser(HttpContext);

            Organization org = new Organization();
            org.Manager = currentUser;
            org.ManagerId = currentUser.Id;

            return View(org);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name,Country,Video,Description,ManagerId")] Organization organization)
        {
            var currentUser = _usersService.getCurrentLoggedInUser(HttpContext);
            if (currentUser.UserRole == UserRole.ORG_ADMIN)
            {
                return BadRequest();
            }
            if (ModelState.IsValid)
            {
                if(currentUser.UserRole == UserRole.CLIENT)
                    currentUser.UserRole = UserRole.ORG_ADMIN;
                currentUser.OrganizationId = organization.Id;
                currentUser.Organization = organization;

                _context.Add(organization);
                _context.Update(currentUser);
                await _context.SaveChangesAsync();

                return RedirectToAction(nameof(Index));
            }
            
            organization.Manager = currentUser;
            organization.ManagerId = currentUser.Id;
            return View(organization);
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var organization = await _context.Organization.FindAsync(id);
            if (organization == null)
            {
                return NotFound();
            }


            return View(organization);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name,Country,Video,Description,ManagerId")] Organization organization)
        {
            if (id != organization.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(organization);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!OrganizationExists(organization.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }

            return View(organization);
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var organization = await _context.Organization
                .Include(o => o.Manager)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (organization == null)
            {
                return NotFound();
            }

            return View(organization);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var organization = await _context.Organization.FindAsync(id);
            _context.Organization.Remove(organization);

            var currentUser = _usersService.getCurrentLoggedInUser(HttpContext);
            if(currentUser.UserRole == UserRole.ORG_ADMIN)
                currentUser.UserRole = UserRole.CLIENT;
            
            _context.Update(currentUser);
            
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool OrganizationExists(int id)
        {
            return _context.Organization.Any(e => e.Id == id);
        }

        public async Task<IActionResult> Search(string? name, string? country)
        {
            var query = from organization in _context.Organization
                        where (name != null ? organization.Name.ToLower().Contains(name.ToLower()) : true) &&
                              (country != null ? organization.Country.ToLower() == country.ToLower() : true)
                        select organization;

            List<Organization> foundOrganizations = await query.Include(o => o.Manager)
                                                                .Include(o => o.UserOrganizations)
                                                                .ThenInclude(uo => uo.User).ToListAsync();
            
            int? curUserId = _usersService.getCurrentLoggedInUserId(HttpContext);

            OrgnizationForUser searchResult = new OrgnizationForUser
            {
                Follwing = foundOrganizations.Where(org => org.UserOrganizations.Any(uo => uo.UserId == curUserId)),
                Others = foundOrganizations.Where(org => org.ManagerId != curUserId && (org.UserOrganizations.Count() == 0 || org.UserOrganizations.All(uo => uo.UserId != curUserId)))
            };

            ViewData["SearchedName"] = name;
            ViewData["SearchedCountry"] = country;

            return View("Index", searchResult);
        }
        
        [HttpPost]
        public async Task<IActionResult> Join(int? orgId)
        {
            if (orgId == null || !OrganizationExists((int) orgId))
            {
                return NotFound();
            }

            var currentUser = _usersService.getCurrentLoggedInUserExtendedIncludes(HttpContext);

            if (currentUser == null 
                || currentUser.UserOrganizations.Any(org => org.OrganizationId == orgId))
            {
                return Unauthorized();
            }

            var orgToJoin = await _context.Organization
                .Include(e => e.UserOrganizations)
                .ThenInclude(e => e.User)
                .FirstOrDefaultAsync(e => e.Id == orgId);
            
            UserOrganization userOrganization = addUserToOrg(orgToJoin, currentUser);
            orgToJoin.UserOrganizations.Add(userOrganization);
            currentUser.UserOrganizations.Add(userOrganization);
            
            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(orgToJoin);
                    _context.Update(currentUser);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    throw;
                }

                return RedirectToAction(nameof(Index));
            }

            return View("Details", orgToJoin);
        }

        private UserOrganization addUserToOrg(Organization org, User u)
        {
            UserOrganization userOrganization = new UserOrganization();
            
            userOrganization.Organization = org;
            userOrganization.OrganizationId = org.Id;
            userOrganization.User = u;
            userOrganization.UserId = u.Id;

            return userOrganization;
        }
        
        [HttpPost]
        public async Task<IActionResult> UnJoin(int? orgId)
        {
            if (orgId == null || !OrganizationExists((int) orgId))
            {
                return NotFound();
            }

            var currentUser = _usersService.getCurrentLoggedInUserExtendedIncludes(HttpContext);

            if (currentUser == null 
                || !currentUser.UserOrganizations
                    .Any(org => org.OrganizationId == orgId))
            {
                return Unauthorized();
            }

            var orgToUnjoin = await _context.Organization
                .Include(e => e.UserOrganizations)
                .ThenInclude(e => e.User)
                .FirstOrDefaultAsync(e => e.Id == orgId);
            
            List<ICollection<UserOrganization>> updated = removeUserFromOrg(orgToUnjoin, currentUser);
            
            orgToUnjoin.UserOrganizations = updated[0];
            currentUser.UserOrganizations = updated[1];
            
            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(orgToUnjoin);
                    _context.Update(currentUser);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    throw;
                }

                return RedirectToAction(nameof(Index));
            }

            return View("Details", orgToUnjoin);
        }

        private List<ICollection<UserOrganization>> removeUserFromOrg(Organization org, User u)
        {

            var orgUserOrgs = org.UserOrganizations;
            UserOrganization toRemoveOne = orgUserOrgs.First(uo => uo.UserId == u.Id);
            orgUserOrgs.Remove(toRemoveOne);
            
            var userOrgs = u.UserOrganizations;
            UserOrganization toRemoveTwo = userOrgs.First(uo => uo.OrganizationId == org.Id);
            userOrgs.Remove(toRemoveTwo);

            List<ICollection<UserOrganization>> result 
                = new List<ICollection<UserOrganization>>();
            result.Add(orgUserOrgs);
            result.Add(userOrgs);

            return result;
        }

        [Authorize]
        public IActionResult MyOrganization()
        {
            User currentUser = _usersService.getCurrentLoggedInUser(HttpContext);

            return View(currentUser.Organization);
        }
    }
}
