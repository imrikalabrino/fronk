﻿using fronk.Data;
using fronk.Extenstions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace fronk.Models
{
    /*[Authorize(Roles = "SYS_ADMIN")]*/
    /*[UserRoleAuthorize(UserRole.SYS_ADMIN)]*/
    public class AdministrationController : Controller
    {
        private readonly fronkContext _context;

        public AdministrationController(fronkContext context)
        {
            _context = context;
        }

        public IActionResult Users()
        {
            return RedirectToAction("Index", "Users");
            //return View("Index", "UsersController");
        }
        public IActionResult Statistics()
        {
            return View();
        }

        public List<User> Hello()
        {
            List<User> users = (from user in _context.User
                                select user).ToList();
            return users;
        }

        public JsonResult GetOrganizationsGroupedByCountry()
        {
            var query = from org in _context.Organization
                        group org by org.Country into country
                        select new
                        {
                            name = country.Key,
                            count = country.Count()
                        };

            List<Object> result = query.ToList<object>();

            return Json(result);
        }

        public JsonResult GetMostPopularEvents(int count)
        {
            count = count >= 1 ? count : 3;

            List<Event> events = _context.Event.Include(e => e.Users).ToList();

            IEnumerable<Object> result = events.Aggregate(new List<Object>(), (acc, cur) => {
                Object item = new
                {
                    title = cur.Title,
                    userCount = cur.Users.Count()
                };

                acc.Add(item);

                return acc;
            }).Take(count);

            return Json(result);
        }
    }
}
