﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using fronk.Data;
using fronk.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using System.Security.Claims;

namespace fronk.Controllers
{
    public class LoginController : Controller
    {
        private readonly fronkContext _context;

        public LoginController(fronkContext context)
        {
            _context = context;
        }

        public IActionResult AccessDenied()
        {
            return View();
        }

        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);

            return RedirectToAction("Index");
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Index([Bind("Id,Name,Email,Password,UserRole,CreatedAt")] User user)
        {
            if (ModelState.IsValid)
            {
                User foundUser = _context.User.FirstOrDefault(u => u.Name == user.Name && u.Password == user.Password);

                if (foundUser == null)
                {
                    ViewData["ErrorMessage"] = "Oysh, you are missing something...";
                }
                else
                {
                    this.Signin(foundUser);
                    return RedirectToAction(nameof(Index), "Home");
                }
            }

            return View(user);
        }

        private Boolean checkCredentails(string username, string password)
        {
            var matchingUsers = _context.User.Where(user => user.Name == username && user.Password == password);

            return (matchingUsers.Count() == 1);
        }

        private async void Signin(User user)
        {
            List<Claim> claims = new List<Claim> { 
                new Claim(ClaimTypes.Name, user.Name),
                new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
                new Claim(ClaimTypes.Role, user.UserRole.ToString())
            };
            ClaimsIdentity claimsIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
            AuthenticationProperties authProperties = new AuthenticationProperties {
                ExpiresUtc = DateTimeOffset.UtcNow.AddMinutes(10)
            };
            
            await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(claimsIdentity),authProperties);
        }

        public IActionResult Register()
        {
            ViewData["Gender"] = new SelectList(Enum.GetValues(typeof(Gender)));

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register([Bind("Id,Name,Email,Password,UserRole,CreatedAt")] User user)
        {
            if (ModelState.IsValid)
            {
                if (isUsernameTaken(user.Name))
                {
                    ViewData["ErrorMessage"] = "Oops... You'll have to be more creative!";
                }
                else
                {
                    user.UserRole = UserRole.CLIENT;
                    user.CreatedAt = new DateTime();

                    _context.Add(user);
                    await _context.SaveChangesAsync();

                    this.Signin(user);

                    return RedirectToAction(nameof(Index), "Home");
                }
            }

            return View(user);
        }


        private Boolean isUsernameTaken(string username) 
        {
            User existingUser = this._context.User.FirstOrDefault(user => user.Name == username);
            return existingUser != null;
        }
    }
}
