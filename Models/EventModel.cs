using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace fronk.Models
{
    public class Event
    {
        [Key] public int Id { get; set; }

        public int OrganizationId { get; set; }
        public Organization Organization { get; set; }

        [StringLength(200, MinimumLength = 2)]
        [DataType(DataType.Text)]
        [Required]
        public string Title { get; set; }

        [Required]
        public string Description { get; set; }
        
        [Required]
        public Location Location { get; set; }
         
        public int EventCategoryId { get; set; }
        
        public EventCategory EventCategory { get; set; }

        public DateTime CreatedAt { get; set; }

        [DataType(DataType.DateTime)] 
        [Required]
        public DateTime Date { get; set; }

        public string ImageSrc { get; set; }

        public List<User> Users { get; set; }
    }
}