using System.ComponentModel.DataAnnotations;

namespace fronk.Models
{
    public class Location
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string PlaceId { get; set; }
        
        [Required]
        public string Name { get; set; }
        
        public int EventId { get; set; }
        public Event Event { get; set; }
    }
}