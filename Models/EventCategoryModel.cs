﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace fronk.Models
{
    public class EventCategory
    {
        public int Id { get; set; }

        [Required]
        [StringLength(150, MinimumLength = 3)]
        public string Name { get; set; }

        [RegularExpression("^#([a-fA-F0-9]{6}|[a-fA-F0-9]{3})$")]
        public string Color { get; set; }

        public List<Event> Events { get; set; }
    }
}