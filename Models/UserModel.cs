using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace fronk.Models
{
    public enum UserRole
    {
        CLIENT,
        ORG_ADMIN,
        SYS_ADMIN
    }

    public enum Gender
    {
        MALE,
        FEMALE,
        OTHER
    }

    public class User
    {
        public int Id { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }

        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [DataType(DataType.Password)]
        [Required]
        public string Password { get; set; }

        public UserRole UserRole { get; set; }

        public DateTime CreatedAt { get; set; }

        [DataType(DataType.Date)]
        public DateTime DateOfBirth { get; set; }

        public Gender Gender { get; set; }

        public ICollection<UserOrganization> UserOrganizations { get; set; }

        public int? OrganizationId { get; set; }

        public Organization Organization { get; set; } 
        
        public List<Event> Events { get; set; }
    }
}