using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace fronk.Models
{
    public class Organization
    {
        public int Id { get; set; }

        public string Name { get; set; }

        // ISO 3166-1 code
        [RegularExpression(
            "^A[^ABCHJKNPVY]|B[^CKPUX]|C[^BEJPQST]|D[EJKMOZ]|E[CEGHRST]|F[IJKMOR]|G[^CJKOVXZ]|H[KMNRTU]|I[DEL-OQ-T]|J[EMOP]|K[EGHIMNPRWYZ]|L[ABCIKR-VY]|M[^BIJ]|N[ACEFGILOPRUZ]|OM|P[AE-HK-NRSTWY]|QA|R[EOSUW]|S[^FPQUW]|T[^ABEIPQSUXY]|U[AGMSYZ]|V[ACEGINU]|WF|WS|YE|YT|Z[AMW]$"
        )]
        [DisplayName("Organization country (leave empty if international)")]
        public string Country { get; set; }

        public string? Video { get; set; }

        public string Description { get; set; }

        public int? ManagerId { get; set; }

        public User Manager { get; set; }

        public ICollection<UserOrganization> UserOrganizations { get; set; }

        public List<Event> Events { get; set; }
    }
}